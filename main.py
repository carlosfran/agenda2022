from flask import Flask, g, request, render_template, \
    redirect, session, url_for, flash

import mysql.connector

from models.agenda import Agenda
from models.contato import Contato
from models.agendaDAO import AgendaDAO
from models.contatoDAO import ContatoDAO

app = Flask(__name__)
app.config.from_pyfile("config.py")

app.auth = {
    # acao: { perfil:permissao }
    'cadastrar_contato': {0:1, 1:1},
    'listar_contatos': {0:1, 1:1},
    'deletar_contato': {0:1, 1:1},
    'logout': {0:1, 1:1},
    'painel': {0:1, 1:1},
    'listar_usuarios': {0: 0, 1: 1},
    'atualizar_contato': {0:1, 1:1}
}


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = mysql.connector.connect(
            host=app.config['DBHOST'],
            user=app.config['DBUSER'],
            password=app.config['DBPASS'],
            database=app.config['DBNAME']
        )
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/cadastrar', methods=['GET', 'POST', ])
def cadastrar():
    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        telefone = request.form['telefone']
        senha = request.form['senha']
        perfil = 0
        agenda = Agenda(nome, email, senha, telefone, perfil)
        dao = AgendaDAO(get_db())
        id = dao.inserir(agenda)
        if id > 0:
            flash(f'Cadastrado com sucesso! ID = %d' % id, 'success')
        else:
            flash(f'Erro ao cadastrar agenda/usuario!', 'danger')

    return render_template('cadastrar.html')


@app.route('/cadastrar_contato', methods=['GET', 'POST', ])
def cadastrar_contato():
    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        telefone = request.form['telefone']

        contato = Contato(nome, telefone, email)
        contato.setIdAgenda(session['logado']['id'])
        dao = ContatoDAO(get_db())
        id = dao.inserir(contato)
        if id > 0:
            flash(f'Contato (id = %d) cadastrado com sucesso!' % id, 'success')
        else:
            flash(f'Erro ao cadastrar contato!', 'danger')

    return render_template('cadastrar-contato.html')

@app.route('/listar_contatos', methods=['GET',])
def listar_contatos():
    dao = ContatoDAO(get_db())
    meus_contatos = dao.listar(session['logado']['id'])
    return render_template('listar-contatos.html', contatos=meus_contatos)

@app.route('/deletar_contato/<id_contato>', methods=['GET',])
def deletar_contato(id_contato):
    dao = ContatoDAO(get_db())
    ret = dao.deletar(session['logado']['id'], id_contato)
    if ret>0:
        flash(f'Contato {id_contato} excluído!', 'success')
    else:
        flash(f'Erro ao excluído contato {id_contato}!', 'danger')
    return redirect(url_for('listar_contatos'))

@app.route('/atualizar_contato/<id_contato>', methods=['GET', 'POST'])
def atualizar_contato(id_contato):
    dao = ContatoDAO(get_db())
    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        telefone = request.form['telefone']

        contato = Contato(nome, telefone, email)
        contato.setIdAgenda(session['logado']['id'])
        contato.setId(id_contato)

        ret = dao.atualizar(contato)
        if ret > 0:
            flash(f'Contato (id = %s) foi atualizado com sucesso!' % id_contato, 'success')
        else:
            flash(f'Erro ao atualizar contato!', 'danger')

    contato = dao.obter(session['logado']['id'], id_contato)
    return render_template('atualizar-contato.html', c=contato)

@app.route('/login', methods=['GET', 'POST', ])
def login():
    if request.method == 'POST':
        email = request.form['email']
        senha = request.form['senha']
        dao = AgendaDAO(get_db())
        usuario = dao.autenticar(email, senha)
        if usuario is not None:
            session['logado'] = {'id': usuario[0],
                                 'nome': usuario[1],
                                 'email': usuario[2],
                                 'telefone': usuario[4],
                                 'perfil': usuario[5]}
            return redirect(url_for('painel'))
        else:
            flash(f'Erro ao efetuar login!', 'danger')

    return render_template('login.html')


@app.route('/logout')
def logout():
    session['logado'] = None
    session.clear()
    return redirect(url_for('index'))


@app.route('/painel')
def painel():
    return render_template('painel.html')


@app.route('/listar_usuarios', methods=['GET',])
def listar_usuarios():
    dao = AgendaDAO(get_db())
    usuarios = dao.listar()
    return render_template('listar-usuarios.html', usuarios=usuarios)


@app.before_request
def autorizacao():
    acao = request.path[1:]
    acao = acao.split('/')
    if len(acao)>=1:
        acao = acao[0]

    acoes = app.auth.keys()
    if acao in list(acoes):
        if session.get('logado') is None:
            return redirect(url_for('login'))
        else:
            perfil = session['logado']['perfil']
            if app.auth[acao][perfil]==0:
                return redirect(url_for('painel'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
