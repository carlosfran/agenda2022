class Contato:
    def __init__(self, nome, telefone, email):
        self.id = 0
        self.id_agenda = 0
        self.nome = nome
        self.telefone = telefone
        self.email = email

    def setId(self, id):
        self.id = id

    def getId(self):
        return self.id

    def setIdAgenda(self, id_agenda):
        self.id_agenda = id_agenda

    def getIdAgenda(self):
        return self.id_agenda

    def to_object(self, dados:dict):
        contato = Contato(dados['nome'],
                          dados['telefone'], dados['email'])

        return contato


    def to_dict(self, contato):
        dic = {'nome': contato.nome,
               'telefone': contato.telefone,
               'email': contato.email }
        return dic



