class Agenda:
    def __init__(self, nome, email, senha, telefone, perfil):
        self.id = 0
        self.nome = nome
        self.email = email
        self.senha = senha
        self.telefone = telefone
        self.perfil = perfil


    def setId(self, id):
        self.id = id

    def getId(self):
        return self.id

    def to_object(self, dados:dict):
        agenda = Agenda(dados['nome'], dados['email'],
                        dados['senha'], dados['telefone'],
                        dados['perfil'])

        return agenda


    def to_dict(self, agenda):
        dic = {'nome': agenda.nome,
               'email': agenda.email,
               'senha': agenda.senha,
               'telefone': agenda.telefone,
               'perfil': agenda.perfil}
        return dic









